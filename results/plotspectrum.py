import argparse
import csv
import matplotlib.pyplot as plt
import numpy as np

from parameters import *


# This is to pass files other than data.csv.
# It will default to the file data.csv.
parser = argparse.ArgumentParser()
parser.add_argument("file", type=str, nargs='?',
                    default="data.csv",
                    help="The CSV files. The default is data.csv.")
args = parser.parse_args()
file = args.file

# Get the data.
energies = []
with open(file) as temp:
    reader = csv.reader(temp)
    for row in reader:
        energy = float(row[0])
        energies.append(energy)
energies.sort()

# Get the channels.
num_channels=200
max_energy = energies[-1]
channels = np.linspace(0.0, max_energy, num_channels+1)
channel_width = channels[1] - channels[0]
channels = channels[:-1] + channel_width

# Get the intensities.
intensities = np.zeros(num_channels)
idx = 0 #The index of the channel.
for energy in energies:
    while energy > channels[idx] + channel_width:
        idx += 1 #Get to the next channel.
    intensities[idx] += energy
intensities /= channel_width
intensities /= detector_area

# Plot the spectrum.
plt.plot(channels, intensities)
plt.xlim(0.)
plt.ylim(0.)
plt.xlabel("energy at detector (keV)")
plt.ylabel("deposited-energy density per second (1/s)")
plt.show()
