import math as ma


num_electrons = 10000000 #100000000
current = 1e-3 #amp
detector_diameter = 30. #mm
height = 35. #mm
opening_angle = 2. * ma.atan(height/detector_diameter) * 1000. #mrad
detector_area = ma.pi*(detector_diameter/2.)**2. #mm^2
