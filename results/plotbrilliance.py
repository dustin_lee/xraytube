import argparse
import csv
import math as ma
import matplotlib.pyplot as plt
import numpy as np

from parameters import *

# This is to pass files other than data.csv.
# It will default to the file data.csv.
parser = argparse.ArgumentParser()
parser.add_argument("file", type=str, nargs='?',
                    default="data.csv",
                    help="The CSV file. The default is data.csv.")
args = parser.parse_args()
file = args.file

# Get the data.
energies = []
with open(file) as temp:
    reader = csv.reader(temp)
    for row in reader:
        energy = float(row[0])
        energies.append(energy)

# Compute the brilliances.
central_energies = np.linspace(0., max(energies), 500)
brilliances = np.zeros(central_energies.shape)
for i, central_energy in enumerate(central_energies):
    # Get the counts within the bandwidth.
    # energy_width = 1.2835936544184*.001*central_energy
    energy_width = .001*central_energy
    counts = 0
    for energy in energies:
        if abs(energy-central_energy) <= energy_width/2.:
             counts += 1
    # Weight the counts by the current.
    num_electrons_per_second = current*(1./1.602176634e-19)
    counts_per_second = counts*(num_electrons_per_second/num_electrons)
    brilliance = counts_per_second/(detector_area*opening_angle**2.)
    # Append the brilliance.
    brilliances[i] = brilliance

# Plot the brilliances.
plt.plot(central_energies, brilliances)
plt.xlim(0.)
plt.ylim(0.)
plt.xlabel("energy (keV)")
plt.ylabel("brilliance ((count/s)/(mm^2⋅mrad^2))")
plt.show()
