#include "G4RunManagerFactory.hh"
#include "G4UImanager.hh"
#include "G4UIExecutive.hh"
#include "G4VisManager.hh"
#include "G4VisExecutive.hh"
#include "G4SystemOfUnits.hh"

#include "construction.hh"
#include "physics.hh"
#include "action.hh"


int main(int argc, char** argv) {
  auto* runManager = G4RunManagerFactory::CreateRunManager();
  runManager->SetUserInitialization(new MyConstruction());
  runManager->SetUserInitialization(new MyPhysicsList());
  runManager->SetUserInitialization(new InitialActions());
  runManager->Initialize();

  auto* ui = new G4UIExecutive(argc, argv);

  G4VisManager* visManager = new G4VisExecutive();
  visManager->Initialize();

  auto* uiManager = G4UImanager::GetUIpointer();
  uiManager->ApplyCommand("/vis/open OGL");
  uiManager->ApplyCommand("/vis/viewer/set/viewpointVector 1 1 1");
  uiManager->ApplyCommand("/vis/drawVolume");
  uiManager->ApplyCommand("/vis/scene/add/trajectories smooth");
  uiManager->ApplyCommand("/vis/scene/endOfEventAction accumulate");

  ui->SessionStart();

  return 0;
}
