# Xray Tube Simulation

Author: Dustin Lee Enyeart (dustin.enyeart@protonmail.com)


## About

This project simulates an xray tube.
It uses the framework Geant4.
This project is located [here](https://gitlab.com/dustin_lee/xraytube).
This one of several simulations of xray sources.
More information is in the file ```report.pdf```.


## How to Run

This project was made using version 10.7 of Geant4. These instructions are written for Ubuntu 20.04.3 LTS.

Geant4 can be downloaded from [here](https://geant4.web.cern.ch/support/download). A video tutorial on installing it can be viewed [here](https://yewtu.be/watch?v=Lxb4WZyKeCE).

After downloading Geant4, it needs to be sourced. This can be done by running ```source <location of geant4 source file>``` in the terminal where the simulation will be ran. The tail of the source file should be something similar to be ```geant4/install/share/Geant4-10.7.2/geant4make/geant4make.sh```. This can be added to the file ```.bashrc``` so that this does not need to be done for each new shell.

To run the simulation, make a directory ```build``` in the main folder. In this directory, run ```cmake ..```, and then run ```make```. This will create an executable object ```main```, which can be ran with ```./main```.

In the GUI, run the command ```/run/beamOn <number of electrons>``` to run the simulation. The number ```10000000``` should be passed to agree with the variable ```num_electrons``` in the file ```results/parameters.py```.


## Code Organization

The main function of the simulation is in the file ```main.cpp```. The bulk of the code is in the directory ```source```. The data is saved and analyzed in the directory ```results```.


### Source

Geant4 requires three classes to be registered.
These classes are derived from ```G4VUserPhysicsList```, ```G4VUserActionInitialization``` and ```G4VUserDetectorConstruction```.
The definitions of these derived classes are located in ```physics.hh```, ```action.hh``` and ```construction.hh```, respectively.
They are registered in ```main.cpp``` in the main directory.

Instead of directly using ```G4VUserPhysicsList```, the class ```G4VModularPhysicsList``` is used, which is derived from it.
The Livermore electromagnetic physics list is registered in ```physics.hh```.

The only user-defined action is the initialization of the incident electron. This is defined in the class ```MyGenerator``` in ```generator.hh```, which is registered with ```InitialActions``` in ```action.hh```.

Each object in the construction is made in three steps.
The first step is to define the shape of the object. This is called the *solid* incarnation of the object.
The second step is to define the object, that is, combine the shape and the material of the object. This is the *logical* incarnation of the object.
Finally, the object is placed in a mother volume. This *physical* incarnation of the object.

The detector must be registered as a detector, so that the method ```ProcessHits``` to record data can be defined and called. The detector is registered as an instance of the class ```MyDetector```, which is defined in ```detector.hh```.


### Results

#### General

The data that is recorded by the detector is saved in the file ```results/data.csv```. If parameters are changed in the simulation, then they also need to manually changed in the file ```results/parameters.py```.

If the parameters in the program ```source/parameters.hh``` are changed, then they also need to be manually changed in the program ```results/plotbrilliance.py```.
The parameter ```num_electrons``` in the program ```results/plotbrilliance.py``` is the number that is passed to the command ```run/beamOn``` in the GUI.


#### How to Run

Version 3.9 of Python is used to analyze the data.
It is recommended that a virtual environment is used.
Instructions for setting up a virtual environment for python are [here](https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment).
On GNU/Linux, use
```
python3 -m venv env
source env/bin/activate
pip3 install -r requirements.txt
export PYTHONPATH="${PYTHONPATH}:./source"
```
to set up the virtual environment.
Use ```deactivate``` to leave the virtual environment, and ```source env/bin/activate``` to reenter it.
