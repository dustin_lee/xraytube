#ifndef GENERATOR_HH
#define GENERATOR_HH 0

#include "G4VUserPrimaryGeneratorAction.hh"
#include "G4ParticleGun.hh"
#include "G4SystemOfUnits.hh"
#include "G4Electron.hh"


class MyGenerator : public G4VUserPrimaryGeneratorAction {
public:
  MyGenerator() {
    particleGun = new G4ParticleGun(1);
  }
  ~MyGenerator() {
    delete particleGun;
  }
  G4ParticleGun* particleGun;
private:
  virtual void GeneratePrimaries(G4Event* event) {
    particleGun->SetParticleDefinition(G4Electron::Definition());
    particleGun->SetParticlePosition(G4ThreeVector(-4.9*cm, 2.0*cm, 0.0*cm));
    particleGun->SetParticleMomentumDirection(G4ThreeVector(1.0, 0.0, 0.0));
    particleGun->SetParticleEnergy(200.0*keV);
    particleGun->GeneratePrimaryVertex(event);
  }
};


#endif //TEST_HH
