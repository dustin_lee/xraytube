#ifndef CONSTRUCTION_HH
#define CONSTRUCTION_HH 0

#include "G4VUserDetectorConstruction.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4NistManager.hh"
#include "G4SDManager.hh"
#include "G4SystemOfUnits.hh"

#include "detector.hh"


class MyConstruction : public G4VUserDetectorConstruction {
public:
  MyConstruction() {};
  virtual G4VPhysicalVolume* Construct();
};


G4VPhysicalVolume* MyConstruction::Construct() {
  auto* nist = G4NistManager::Instance(); //Material manager that uses data
                                          //from NIST.

  //Make the environment.
  auto* worldSolid = new G4Box("worldSolid", 5.0*cm, 5.0*cm, 5.0*cm);
  auto* worldLogical = new G4LogicalVolume(worldSolid,
                                      nist->FindOrBuildMaterial("G4_Galactic"),
                                      "worldLogical");
  auto worldLocation = G4ThreeVector(0.0, 0.0, 0.0);
  auto* worldPhysical = new G4PVPlacement(NULL, worldLocation,
                                          worldLogical, "WorldPhysical",
                                          NULL,
                                          false, 0, true);

  //Make the target.
  auto* targetSolid = new G4Box("targetSolid", 0.1*cm, 1.0*cm, 1.0*cm);
  auto* targetLogical = new G4LogicalVolume(targetSolid,
                                            nist->FindOrBuildMaterial("G4_W"),
                                            "targetLogical");
  auto targetLocation = G4ThreeVector(0.0*cm, 2.0*cm, 0.0*cm);
  auto* targetRot = new G4RotationMatrix();
  targetRot->rotateZ(-45.0*deg);
  new G4PVPlacement(targetRot, targetLocation,
                    targetLogical, "targetPhysical",
                    worldLogical,
                    false, 0, true);

  // Replace the lower part with air.
  // auto distTargetToWall = 2.0*cm;
  // auto outsideThickness = 5.0*cm;
  // auto outsideHeight = -5.0*cm + outsideThickness/2.0;
  auto* outsideSolid = new G4Box("outsideSolid",
                                 5.0*cm, 5.0*cm/2.0, 5.0*cm);
  auto* outsideLogical = new G4LogicalVolume(outsideSolid,
                                          nist->FindOrBuildMaterial("G4_AIR"),
                                          "outsideLogical");
  auto outsideLocation = G4ThreeVector(0.0*cm, -2.5*cm, 0.0*cm);
  new G4PVPlacement(NULL, outsideLocation,
                    outsideLogical, "outsidePhysical",
                    worldLogical,
                    false, 0, true);

  // Make the wall and window.
  auto wallThickness = 0.1*cm;
  // First, make the material of the wall.
  auto* steel = new G4Material("steel",
                               7.9*g/cm3,  //Density
                               2);         //Number of components.
  steel->AddMaterial(nist->FindOrBuildMaterial("G4_Fe"), 97.9*perCent);
  steel->AddMaterial(nist->FindOrBuildMaterial("G4_C"), 2.1*perCent);
  // Make the wall.
  auto* wallSolid = new G4Box("wallSolid", 5.0*cm, 5.0*cm, wallThickness/2.0);
  auto* wallLogical = new G4LogicalVolume(wallSolid,
                                          steel,
                                          "wallLogical");
  auto wallLocation = G4ThreeVector(0.0*cm, 2.5*cm - wallThickness/2.0,
                                    0.0*cm);
  auto* wallRot = new G4RotationMatrix();
  wallRot->rotateX(90.0*deg);
  new G4PVPlacement(wallRot, wallLocation,
                    wallLogical, "wallPhysical",
                    outsideLogical,
                    false, 0, true);
  // Now, replace part of the wall with the window.
  auto windowDiameter = 2.0*cm;
  auto* windowSolid = new G4Tubs("windowSolid",
                                 0.0*cm, windowDiameter,//Inner and outer radii
                                 wallThickness/2.0,     //Thickness
                                 0.0*deg, 360.0*deg);   //Angles of rotation.
  auto* windowLogical = new G4LogicalVolume(windowSolid,
                                            nist->FindOrBuildMaterial("G4_Be"),
                                            "windowLogical");
  auto windowLocation = G4ThreeVector(0.0*cm, 0.0*cm, 0.0*cm);
  new G4PVPlacement(NULL, windowLocation,
                    windowLogical, "windowPhysical",
                    wallLogical,
                    false, 0, true);

  //Make the detector.
  auto detectorDiameter = 2.0*cm;
  auto* detectorSolid = new G4Tubs("detectorSolid",
                                   0.0*cm, detectorDiameter, //Radii
                                   0.001*cm,                 //Thickness
                                   0.0*deg, 360.0*deg);      //Angles
  auto* detectorLogical = new G4LogicalVolume(detectorSolid,
                                      nist->FindOrBuildMaterial("G4_Galactic"),
                                      "detectorLogical");
  auto* detectorRot = new G4RotationMatrix();
  detectorRot->rotateX(90.0*deg);
  auto detectorLocation = G4ThreeVector(0.0*cm, 0.0*cm, 0.0*cm);
  new G4PVPlacement(detectorRot, detectorLocation,
                    detectorLogical, "detectorPhysical",
                    outsideLogical,
                    false, 0, true);
  // Register the detector.
  auto* SdManager = G4SDManager::GetSDMpointer();
  auto* detector = new MyDetector("detector");
  SdManager->AddNewDetector(detector);
  detectorLogical->SetSensitiveDetector(detector);

  return worldPhysical;
}


#endif //TEST_HH
