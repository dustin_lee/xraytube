#ifndef DETECTOR_HPP
#define DETECTOR_HPP 0

#include <fstream>

#include "G4VSensitiveDetector.hh"


class MyDetector : public G4VSensitiveDetector {
public:
  MyDetector(G4String name) : G4VSensitiveDetector(name) {}

  virtual G4bool ProcessHits(G4Step* step, G4TouchableHistory* hist) {
    G4Track* track = step->GetTrack();
    track->SetTrackStatus(fKillTrackAndSecondaries);

    auto particleName = track->GetParticleDefinition()->GetParticleName();
    if (particleName == "gamma") {
      auto dataFile = std::ofstream("../results/data.csv",
                                    std::ios_base::app);
      auto* stepPoint = step->GetPreStepPoint();
      dataFile << stepPoint->GetKineticEnergy()/keV << '\n';
      dataFile.close();
    }

    return true;
  }
};


#endif //DETECTOR_HPP
