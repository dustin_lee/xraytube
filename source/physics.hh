#ifndef PHYSICS_HH
#define PHYSICS_HH 0

#include "G4VModularPhysicsList.hh"
#include "G4EmLivermorePhysics.hh"


class MyPhysicsList : public G4VModularPhysicsList {
public:
  MyPhysicsList() {
    RegisterPhysics(new G4EmLivermorePhysics());
    // If the cut value for gamma rays is not set below the default,
    // then xrays will not be produced.
    SetCutValue(.00001*mm, "gamma");
  }
};


#endif //TEST_HH
