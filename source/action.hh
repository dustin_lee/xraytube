#ifndef ACTION_HH
#define ACTION_HH 0

#include "G4VUserActionInitialization.hh"

#include "generator.hh"


class InitialActions : public G4VUserActionInitialization {
public:
  InitialActions() {}
  virtual void Build() const {
    auto* generator = new MyGenerator();
    SetUserAction(generator);
  }
};


#endif //TEST_HH
